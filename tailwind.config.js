module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Sora", "sans-serif"],
      },
      keyframes: {
        kanankiri: {
          "0%, 100%": {
            transform: "translateX(-10px)",
          },
          "50%": {
            transform: "translateX(10px)",
          },
        },
      },
      animation: {
        "kanan-kiri": "kanankiri 5s ease-in-out infinite",
      },
    },
  },
  plugins: [],
};
